package com.example.loadbalancer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.lb.config.RibbonConfiguration;

@SpringBootApplication
@RestController
@RibbonClient(name = "userservice", configuration = RibbonConfiguration.class)
public class SpringLoadBalancerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLoadBalancerApplication.class, args);
	}
	
	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Autowired
	RestTemplate restTemplate;
	

	@RequestMapping(value="/getusers")
	public String getUser() {
		 System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<");
		 String users = this.restTemplate.getForObject("http://userservice/spring-rest/users", String.class);
		 return users;
	}
	
	@RequestMapping(method = RequestMethod.GET , value="/searchuser/{id}")
	public String searchUserByName(@PathVariable("id") String param) {
		 String user = this.restTemplate.getForObject("http://userservice/spring-rest/user/name/"+param, String.class);
		 return user;
	}
	
}
